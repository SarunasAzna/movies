from django.urls import path
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    path('movies/', views.movies, name='movies'),
    path('', RedirectView.as_view(pattern_name="movies", permanent=False))
]

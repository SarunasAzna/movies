import pytest
from movies.ghibli.ghibli_client import DEFAULT_URL

TEST_ITEM_ID = "some_id"
BASE_FILMS_LIST_PATH = "/films"
BASE_PEOPLE_LIST_PATH = "/people"

LIST_TEST_DATA = [
    ({}, ""),
    ({"limit": 50}, "?limit=50"),
    ({"fields": "id,role"}, "?fields=id,role"),
    ({"fields": "id", "limit": 50}, "?limit=50&fields=id")
]
GET_TEST_DATA = [
    ({}, ""),
    ({"limit": 50}, ""),
    ({"fields": "id,role"}, "?fields=id,role"),
    ({"fields": "id", "limit": 50}, "?fields=id")
]


def _test_api_method(method_name, args, kwargs, expected_path, _gh_client, _mock_requests):
    expected_url = DEFAULT_URL + expected_path
    patch = _mock_requests.get(expected_url)
    getattr(_gh_client, method_name)(*args, **kwargs)
    assert patch.called_once


@pytest.mark.api_client
@pytest.mark.parametrize("kwargs, query_string", LIST_TEST_DATA)
def test_get_films_list(kwargs, query_string, gh_client, mock_requests):
    expected_path = BASE_FILMS_LIST_PATH + query_string
    _test_api_method("get_films_list", [], kwargs, expected_path, gh_client, mock_requests)


@pytest.mark.api_client
@pytest.mark.parametrize("kwargs, query_string", LIST_TEST_DATA)
def test_get_people_list(kwargs, query_string, gh_client, mock_requests):
    expected_path = BASE_PEOPLE_LIST_PATH + query_string
    _test_api_method("get_people_list", [], kwargs, expected_path, gh_client, mock_requests)


@pytest.mark.api_client
@pytest.mark.parametrize("kwargs, query_string", GET_TEST_DATA)
def test_get_films_item(kwargs, query_string, gh_client, mock_requests):
    expected_path = "{}/{}{}".format(BASE_FILMS_LIST_PATH, TEST_ITEM_ID, query_string)
    _test_api_method("get_films_item", [TEST_ITEM_ID], kwargs, expected_path, gh_client, mock_requests)


@pytest.mark.api_client
@pytest.mark.parametrize("kwargs, query_string", GET_TEST_DATA)
def test_get_people_item(kwargs, query_string, gh_client, mock_requests):
    expected_path = "{}/{}{}".format(BASE_PEOPLE_LIST_PATH, TEST_ITEM_ID, query_string)
    _test_api_method("get_people_item", [TEST_ITEM_ID], kwargs, expected_path, gh_client, mock_requests)


@pytest.mark.api_client
def test_post(gh_client, mock_requests):
    url = "/post_something"
    patch = mock_requests.post("{}/post_something".format(DEFAULT_URL))
    gh_client.post(url)
    assert patch.called_once


@pytest.mark.api_client
def test_can_use_full_url(gh_client, mock_requests):
    url = "/post_something"
    full_url = "{}/post_something".format(DEFAULT_URL)
    patch = mock_requests.get(full_url)
    gh_client.get(url)
    gh_client.get(full_url)
    assert patch.call_count == 2

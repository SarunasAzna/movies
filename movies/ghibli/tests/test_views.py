import pytest
from movies.ghibli.ghibli_client import DEFAULT_URL
from movies.ghibli.utils import get_films_people_dataframe


LIST_URL = "/movies/"

# id, title
FILM = "film_id", "Oscar is going Wild"
SECOND_FILM = "film2", "Franchise much"

# id, name
PERSON = "person_id", "Sir Film alot"
SECOND_PERSON = "second_person_id", "The Side Kick"

PEOPLE_URL = "{}/people?limit=250&fields=name,films,url".format(DEFAULT_URL)
FILM_URL_TEMPLATE = DEFAULT_URL + "/films/{}?fields=title"


def _get_person_response(person, films):
    return {
        "id": person[0],
        "name": person[1],
        "films": ["{}/films/{}".format(DEFAULT_URL, film[0]) for film in films],
        "url": "{}/people/{}".format(DEFAULT_URL, person[0])
    }


def _get_film_response(film):
    return {"title": film[1]}


@pytest.mark.e2e
def test_list_one_film(client, mock_requests):
    people_response = [_get_person_response(PERSON, [FILM])]
    film_url = FILM_URL_TEMPLATE.format(FILM[0])
    film_response = _get_film_response(FILM)
    mock_requests.get(PEOPLE_URL, json=people_response)
    mock_requests.get(film_url, json=film_response)
    res = client.get(LIST_URL)
    assert res.status_code == 200


@pytest.mark.data_structure
@pytest.mark.parametrize("films, people", [
    ([FILM], [PERSON]),
    ([FILM, SECOND_FILM], [PERSON]),
    ([FILM], [PERSON, SECOND_PERSON]),
    ([FILM, SECOND_FILM], [PERSON, SECOND_PERSON]),
])
def test_one_film_dataframe(films, people, mock_requests):
    people_response = [_get_person_response(person, films) for person in people]
    mock_requests.get(PEOPLE_URL, json=people_response)
    for film in films:
        film_url = FILM_URL_TEMPLATE.format(film[0])
        film_response = _get_film_response(film)
        mock_requests.get(film_url, json=film_response)
    films_df = get_films_people_dataframe()
    assert len(films_df) == len(films)
    error_msg_postfix = " is not on the dataframe, df: {}".format(films_df)
    for i, film in enumerate(films):
        film_col = films_df["film"][i]
        film_link = people_response[0]["films"][i]
        assert film[1] in film_col, "Title{}".format(error_msg_postfix)
        assert film_link in film_col, "film link{}".format(error_msg_postfix)
        for j, person in enumerate(people):
            people_col = films_df["people"][i]
            person_link = people_response[j]["url"]
            assert person[1] in people_col, "person name{}".format(error_msg_postfix)
            assert person_link in people_col, "person link{}".format(error_msg_postfix)

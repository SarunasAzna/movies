import pytest
import requests_mock

from movies.ghibli.ghibli_client import GhibliClient


@pytest.yield_fixture
def mock_requests():
    with requests_mock.mock() as mock:
        yield mock


@pytest.fixture
def gh_client():
    return GhibliClient()

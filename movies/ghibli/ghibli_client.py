import requests
import requests_cache
from django.conf import settings


DEFAULT_URL = "https://ghibliapi.herokuapp.com"


requests_cache.install_cache(
    'ghibli_cache',
    backend=settings.REQUEST_CACHE_BACKEND,
    expire_after=settings.REQUEST_CACHE_TIME,
    **settings.REQUEST_CACHE_BACKEND_OPTIONS
)

GHIBLI_CACHE = requests_cache.get_cache()


class GhibliClient:

    LIST_QUERY_STRINGS = ["limit", "fields"]
    GET_QUERY_STRINGS = ["fields"]

    def __init__(self, url=DEFAULT_URL):
        self._url = url

    def request(self, method, path, **kwargs):
        url = "{}{}".format(self._url, path) if self._url not in path else path
        return getattr(requests, method)(url, **kwargs)

    def get(self, path, **kwargs):
        return self.request("get", path, **kwargs)

    def post(self, path, **kwargs):
        return self.request("post", path, **kwargs)

    @staticmethod
    def _set_query_string(kwargs, query_list):
        query_string = ""
        for key in query_list:
            value = kwargs.pop(key, None)
            if value:
                prefix = "&" if query_string else "?"
                query_string += "{}{}={}".format(prefix, key, value)
        return query_string, kwargs

    def _get_list(self, resource, **kwargs):
        query_string, kwargs = self._set_query_string(kwargs, self.LIST_QUERY_STRINGS)
        path = "/{}{}".format(resource, query_string)
        return self.get(path)

    def _get_one(self, resource, item_id, **kwargs):
        query_string, kwargs = self._set_query_string(kwargs, self.GET_QUERY_STRINGS)
        path = "/{}/{}{}".format(resource, item_id, query_string)
        return self.get(path)

    def get_films_list(self, **kwargs):
        return self._get_list("films", **kwargs)

    def get_films_item(self, resource_id, **kwargs):
        return self._get_one("films", resource_id, **kwargs)

    def get_people_list(self, **kwargs):
        return self._get_list("people", **kwargs)

    def get_people_item(self, resource_id, **kwargs):
        return self._get_one("people", resource_id, **kwargs)

# Create your tasks here
from __future__ import absolute_import, unicode_literals
from django.conf import settings

from celery import shared_task
from movies.ghibli.ghibli_client import GHIBLI_CACHE, GhibliClient

GHIBLI_CLIENT = GhibliClient()


@shared_task(time_limit=settings.TASK_TIME_LIMIT)
def update_films_cache():
    """
    This task goes through all GHIBLI_CACHE requests, deletes and repeats them.
    :return:
    """
    for response_id, response in GHIBLI_CACHE.responses.items():
        url = response[0].url
        GHIBLI_CACHE.delete(response_id)
        GHIBLI_CLIENT.get(url)

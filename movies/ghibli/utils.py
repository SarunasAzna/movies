from pandas import DataFrame

from movies.ghibli.ghibli_client import GhibliClient

COLUMNS = ["film", "people"]


def get_item_link(name, url):
    return '<a href="{}" target="_blank">{}</a>'.format(url, name)


def get_films_people_dataframe():
    g_client = GhibliClient()
    film_dict = {}
    res = g_client.get_people_list(limit=250, fields="name,films,url")
    for person in res.json():
        for film in person.get("films", []):
            film_data = g_client.get(film + "?fields=title")
            film_name = film_data.json().get("title")
            film_link = get_item_link(film_name, film)
            person_link = get_item_link(person.get("name"), person.get("url"))
            if film_link not in film_dict:
                film_dict[film_link] = person_link
            else:
                film_dict[film_link] += ", {}".format(person_link)

    return DataFrame(film_dict.items(), columns=COLUMNS)

from django.http import HttpResponse
from movies.ghibli.utils import get_films_people_dataframe


def movies(request):
    films_people_dataframe = get_films_people_dataframe()
    return HttpResponse(films_people_dataframe.to_html(escape=False))

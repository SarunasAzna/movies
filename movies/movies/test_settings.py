from movies.movies.settings_docker import CELERY_ACCEPT_CONTENT


def test_check_accept_content():
    assert CELERY_ACCEPT_CONTENT == ["json"]

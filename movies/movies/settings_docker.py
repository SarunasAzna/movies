from movies.movies.settings import *  # noqa: F403
from datetime import timedelta
import redis

ALLOWED_HOSTS = ["localhost", "127.0.0.1", "0.0.0.0"]


REDIS_HOST = "redis"  # link on docker-compose network
REDIS_PORT = 6379
request_cache_redis = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

REQUEST_CACHE_BACKEND = "redis"
REQUEST_CACHE_BACKEND_OPTIONS = {"connection": request_cache_redis}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://{}:{}/1".format(REDIS_HOST, REDIS_PORT),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        },
        "KEY_PREFIX": "movies"
    }
}

TASK_TIME_LIMIT = 20
EXPECTED_MOVIES_REQUEST_TIME = 5
CACHE_REFRESH_INTERVAL = REQUEST_CACHE_TIME - EXPECTED_MOVIES_REQUEST_TIME  # noqa: F405
CELERY_CACHE_BACKEND = 'default'
CELERY_BEAT_SCHEDULE = {
    'update_films_cache': {
        'task': 'movies.ghibli.tasks.update_films_cache',
        'schedule': timedelta(seconds=CACHE_REFRESH_INTERVAL),
    },
}

REDIS_SCHEME = 'redis://{}:{}/{}'
REDIS_URL_BROKER = REDIS_SCHEME.format(REDIS_HOST, REDIS_PORT, 1)
REDIS_URL_RESULT = REDIS_SCHEME.format(REDIS_HOST, REDIS_PORT, 2)
REDIS_URL_CACHE = REDIS_SCHEME.format(REDIS_HOST, REDIS_PORT, 3)
CELERY_BROKER_URL = REDIS_URL_BROKER
CELERY_RESULT_BACKEND = REDIS_URL_RESULT
CELERY_RESULT_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = ['json']

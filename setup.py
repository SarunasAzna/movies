#!/usr/bin/env python
from setuptools import setup, find_packages

__version__ = "0.1.0"

setup(
    name="movies",
    author="Sarunas Azna",
    author_email="sarunas@softhog.net",
    version=__version__,
    packages=find_packages(),
    install_requires=[
        "celery==4.4.0",
        "coverage==5.0.3",
        "Django==3.0.3",
        "django-redis==4.11.0",
        "flake8",
        "pandas==1.*",
        "pytest-django==3.8.0",
        "pytest-cov",
        "redis==3.4.1",
        "requests==2.23.0",
        "requests-cache==0.5.2",
        "requests-mock",
    ],
    scripts=['manage.py'],
    entry_points={
        'console_scripts': [
            'movies = manage:main',
        ]
    }
)

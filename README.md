## Movies
This is a simple [Django](https://www.djangoproject.com/) server to list movies from [GHIBLI API](https://ghibliapi.herokuapp.com/).

### Run project with virtualenv
Run these commands on shell (windows and mac setup might differ):
```bash
virtualenv env
source env/bin/activate
pip install -e .
movies runserver
```
You should see films and people list on `http://localhost:8000/movies/`.
Requests to GHIBLI API [cached](movies/ghibli/ghibli_client.py) for 60s with
[requests_cache](https://pypi.org/project/requests-cache/) library. So with this setup
you will get one slow request every 60 seconds since you have to call all third party requests.

### Run project with docker-compose
Make sure that [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) are installed.

Start the server (first time it will take more time because it has do build the images):
```bash
docker-compose up
```
This setup is similar to virtualenv only that now every 55 [cache updated task](movies/ghibli/tasks.py)
is runned with on [celery](http://www.celeryproject.org/).
This way you do not have to renew the cache on request since it is done (or partially done)
on the asynchronous task.

### Additional notes
Run projects tests:
```bash
pip install -r requirements.txt
pytest
```
Run flake8 validation:
```bash
pip install -r requirements.txt
flake8
```
